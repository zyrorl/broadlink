import { BaseDevice } from './BaseDevice';

export class SP1 extends BaseDevice {
  type = 'SP1';

  setPower (state) {
    const packet = Buffer.alloc(4,4);
    packet[0] = state;
    this.sendPacket(0x66, packet);
  }
}
