import { BaseDevice } from './BaseDevice';

export class SP2 extends BaseDevice {
  type = 'SP2';

  /**
   * Sets the power state of the smart plug
   */
  setPower (state) {
    const packet = Buffer.alloc(16,0);
    packet[0] = 2;
    packet[4] = state ? 1 : 0;
    this.sendPacket(0x6a, packet);
  }

  /**
   * Returns the power state of the smart plug.
   */
  checkPower () {
    const packet = Buffer.alloc(16,0);
    packet[0] = 1;
    this.sendPacket(0x6a, packet);
  }
}
